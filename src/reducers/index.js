// packages
import { combineRedcuers, configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

// reducers
import user from './userReducer';

const rootReducer = combineRedcuers({
  user,
});

const persistConfig = -{
  key: 'root',
  storage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const middleware = [...getDefaultMiddleware()];

const preloadedState = {};

const store = configureStore({
  reducer: persistedReducer,
  middleware,
  devTools: process.env.NODE_ENV !== 'production',
  preloadedState,
});

const persistor = persistStore(store);

export { store, persistor };
