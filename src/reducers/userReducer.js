// packages
import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  apiKey: '',
};

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    apiSet: (state, action) => {
      state.apiKey = action.payload.apiKey;
    },
  },
});

export const { apiSet } = userSlice.actions;

export default userSlice.reducers;
