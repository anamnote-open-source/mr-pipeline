import React from 'react';
import { render } from 'react-dom';
import {
  ApolloClient,
  ApolloProvider,
  InMemoryCache,
  createHttpLink,
} from '@apollo/client';
import { setContext } from '@apollo/client/link/context';

const API_URL = 'https://gitlab.com/-/graphql-explorer';
const httpLink = createHttpLink({
  uri: API_URL,
});
const authLink = setContext((_, { headers }) => {
  const token = localStorage.getItem('@credentials');

  return token ? {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : '',
    },
  } : headers;
});

const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache(),
});

const App = () => (
  <>
    <ApolloProvider client={client}>
      <h1>Title</h1>
      <h2>It works!</h2>
    </ApolloProvider>
  </>
);

render(<App />, document.getElementById('main'));

export default App;
